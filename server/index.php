<!DOCTYPE html>

<html>

<head>
  <style>
    .error {
      color: #FF0000;
    }
  </style>
</head>

<body>

  <?php
    $name = $surname = $gender = "";
    $nameErr = $surnameErr = $genderErr = "";

    if($_SERVER["REQUEST_METHOD"] == "POST"){

      if(empty($_POST["name"])){
        $nameErr = "Name is required";
      } else{
        $name = test_input($_POST["name"]);
        if(!preg_match("/^[a-zA-Z]*$/",$name)){
          $name = "";
          $nameErr = "Only letters!";
        }
      }

      if(empty($_POST["surname"])){
        $surnameErr = "Surname is required";
      } else{
        $surname = test_input($_POST["surname"]);
        if(!preg_match("/^[a-zA-Z]*$/",$surname)){
          $surname = "";
          $surnameErr = "Only letters!";
        }
      }

      if(empty($_POST["gender"])){
        $genderErr = "Gender is required";
      } else{
        $gender = test_input($_POST["gender"]);
      }

    }

    function test_input($data){
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }

  ?>

  <h2>Form Test</h2>
  <p><span class="error">* required field</span></p>
  <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
    Name: <input type="text" name="name" value="<?php echo $name;?>">
      <span class="error">*<?php echo $nameErr;?></span>
      <br><br>
    Surname: <input type="text" name="surname" value="<?php echo $surname;?>">
      <span class="error">*<?php echo $surnameErr;?></span>
      <br><br>
    Gender:
      <input type="radio" name="gender"
        <?php if(isset($gender) && $gender == "female") echo "checked";?> value="female">Female
      <input type="radio" name="gender"
        <?php if(isset($gender) && $gender == "male") echo "checked";?> value="male">Male
      <span class="error">*<?php echo $genderErr;?></span>
      <br><br>
    <input type="submit" name="submit" value="Submit">
  </form>

  <?php
    echo "<h2>Your Input:</h2>";
    if($name !== "" && $surname !== "" && $gender !== "")
      echo $name."<br>".$surname."<br>".$gender;
  ?>

</body>

</html>
